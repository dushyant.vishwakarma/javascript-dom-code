// // console.dir(document);  // shows all the properties and methods that are attached to document object

// // show the domain name of website
// // console.log(document.domain);

// // show URL of web page
// console.log(document.URL);

// // show title of web page
// console.log(document.title);

// // show doctype of web page
// console.log(document.doctype);

// // show head section of web page
// console.log(document.head);

// // show body section of web page
// console.log(document.body);

// // show all elements of web page
// console.log(document.all);

// // show all links of web page
// console.log(document.links);

// // show all the images of web page
// console.log(document.images);

// // GET ELEMENT BY ID //
// const headerTitle = document.getElementById('header-title');
// console.log(headerTitle);

// // textContent  (Does not pays attention to any CSS styling)
// console.log(headerTitle.textContent);

// // innerText (pays attention to any CSS styling)
// console.log(headerTitle.innerText);

// // innerHTML adds HTML content
// headerTitle.innerHTML = 'My Item Listener';

// // adding CSS using JS
// const header = document.getElementById('main-header');

// header.style.borderBottom = '5px solid #000';


// GET ELEMENTS BY CLASS NAME
// const items = document.getElementsByClassName('list-group-item');
// console.log(items);
// console.log(items[2]);
// items[1].textContent = 'Changed';
// items[1].style.fontWeight = 'bold';
// items[1].style.backgroundColor = 'yellow';

// cannot do like this, it will give error
// items.style.backgroundColor = '#f4f4f4';

// for(let i = 0; i < items.length; i++){
//     items[i].style.backgroundColor = '#f4f4f4';
// }

// GET ELEMENTS BY TAG NAME
// const li = document.getElementsByTagName('li');

// // ALL LI
// console.log(li);

// // li by index
// console.log(li[3]);

// // change text content
// li[3].textContent = "Changed using JS";

// // change the style
// li[3].style.backgroundColor = 'yellow';

// // change all li's color
// for(let i = 0; i < li.length; i++){
//     li[i].style.backgroundColor = '#f4f4f4';
// }

// QUERY SELECTOR
// const header = document.querySelector('#main-header');
// header.style.borderBottom = 'solid 4px black';

// const input = document.querySelector('input');
// input.placeholder = 'Add an item here';

// // CHANGE SUBMIT BUTTON STYLE
// const submit = document.querySelector('input[type="submit"]');
// submit.value = 'SEND';

// // SELECT LAST ITEM FROM LIST
// const lastItem = document.querySelector('.list-group-item:last-child');
// lastItem.style.color = 'green';

// QUERY SELECTOR ALL
// const titles = document.querySelectorAll('.title');
// titles[0].textContent = "Add your items";

// CHANGE THE COLOR OF ALL ODD ELEMENTS OF ITEM LIST
// const oddItems = document.querySelectorAll('li:nth-child(odd)');

// for(let i = 0; i < oddItems.length; i++){
//     oddItems[i].style.backgroundColor = '#f4f4f4';
// }

// // CHANGE THE COLOR OF ALL ODD ELEMENTS OF ITEM LIST

// const evenItems = document.querySelectorAll('li:nth-child(even)');

// for(let i = 0; i < oddItems.length; i++){
//     evenItems[i].style.backgroundColor = '#ccc';
//     // evenItems[i].style.color = '#fff';
// }

// TRAVERSING THE DOM
// const itemList = document.querySelector('#items');

// PARENT NODE
// console.log(itemList.parentNode);

// // CHANGE COLOR OF PARENT NODE
// itemList.parentNode.style.backgroundColor = '#f4f4f4';

// // CHILD NODES (NOT RECOMMENDED TO USE)
// console.log(itemList.childNodes);

// // CHILDREN (RECOMMENDED TO USE)
// console.log(itemList.children);
// console.log(itemList.children[1]);

// itemList.children[1].style.backgroundColor = 'lightblue';

// // FIRST CHILD (NOT RECOMMENDED TO USE)
// console.log(itemList.firstChild);

// // FIRST ELEMENT CHILD (RECOMMENDED)
// console.log(itemList.firstElementChild);
// itemList.firstElementChild.textContent = 'Hello 1';

// // LAST CHILD (NOT RECOMMENDED TO USE)
// console.log(itemList.lastChild);

// // LAST ELEMENT CHILD (RECOMMENDED)
// console.log(itemList.lastElementChild);
// itemList.lastElementChild.textContent = 'Hello 4';

// nextSibling
// console.log(itemList.nextSibling);

// // nextElementSibling
// console.log(itemList.nextElementSibling);

// // previousSibling
// console.log(itemList.previousSibling);

// // previousElementSibling
// console.log(itemList.previousElementSibling);

// // CREATING ELEMENTS USING JS
// const newDiv = document.createElement('div');

// // ADD CLASS TO THIS DIV
// newDiv.className = 'hello';

// // ADD ID TO THIS DIV
// newDiv.id = 'myDiv';

// // ADD AN ATTRIBUT TO THIS DIV
// newDiv.setAttribute('title','Hello Division');

// // CREATE A TEXT NODE 
// const newDivText = document.createTextNode('Hello Division World');

// // ADD TEXT NODE TO THIS DIV
// newDiv.appendChild(newDivText);

// // ADDING AN ELEMENT TO DOM USING JS
// const container = document.querySelector('header .container');
// const h1 = document.querySelector('header h1');

// console.log(newDiv);

// container.insertBefore(newDiv,h1);

// const newItem = document.createElement('li');
// newItem.className = 'list-group-item';
// newItem.id = 'my-item-id';
// newItem.setAttribute('title','MY LI');
// const newItemText = document.createTextNode('MY LI WORLD');
// newItem.appendChild(newItemText);

// // INSERT IN DOM
// const ulList = document.querySelector('ul .list-group-item');
// const listItems = document.querySelector('ul li');

// console.log(newItem);

// ulList.insertBefore(newItem,listItems[0]);

// EVENT LISTENERS
// CLICK EVENT
// const btn = document.getElementById('button').addEventListener('click',() => console.log("Button Clicked"));

const btn  =document.getElementById('button').addEventListener('click',(e) => {
    // document.getElementById('header-title').textContent = 'Changed';

    // GET PROPERTIES OF TARGET ELEMENT
    // console.log(e.target);
    // console.log(e.target.id);
    // console.log(e.target.className);

    // const output = document.getElementById('output');
    // output.innerHTML = '<h3>Added Item</h3>';

    // TYPE OF EVENT
    // console.log(e.type);

    // GET X AND Y COORDINATES OF MOUSE CURSOR
    // console.log(e.clientX);
    // console.log(e.clienty);

    // GET OFFSET (MEANING  FROM THE DOM ELEMENT) X AND Y COORDINATES
    // console.log(e.offsetX);
    // console.log(e.offsetY);

    // console.log(e.altKey);
    // console.log(e.ctrlKey);
    // console.log(e.shiftKey);



});


// var button = document.getElementById('button');
var box = document.getElementById('box');

//button.addEventListener('click', runEvent);
//button.addEventListener('dblclick', runEvent);
//button.addEventListener('mousedown', runEvent);
//button.addEventListener('mouseup', runEvent);

box.addEventListener('mouseenter', runEvent);
box.addEventListener('mouseleave', runEvent);

box.addEventListener('mouseover', runEvent);
box.addEventListener('mouseout', runEvent);

box.addEventListener('mousemove', runEvent);

// var itemInput = document.querySelector('input[type="text"]');
// var form = document.querySelector('form');
// var select = document.querySelector('select');

// itemInput.addEventListener('keydown', runEvent);
// itemInput.addEventListener('keyup', runEvent);
// itemInput.addEventListener('keypress', runEvent);

// itemInput.addEventListener('focus', runEvent);
// itemInput.addEventListener('blur', runEvent);

// itemInput.addEventListener('cut', runEvent);
// itemInput.addEventListener('paste', runEvent);

// itemInput.addEventListener('input', runEvent);

// select.addEventListener('change', runEvent);
// select.addEventListener('input', runEvent);

// form.addEventListener('submit', runEvent);

function runEvent(e){
//   e.preventDefault();
  console.log('EVENT TYPE: '+e.type);

  //console.log(e.target.value);
//   document.getElementById('output').innerHTML = '<h3>'+e.target.value+'</h3>';

//   output.innerHTML = '<h3>MouseX: '+e.offsetX+' </h3><h3>MouseY: '+e.offsetY+'</h3>';

  document.getElementById('box').style.backgroundColor = "rgb("+e.offsetX+","+e.offsetY+", 40)";
}





















